import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

csv_file = os.getcwd()+'/Salary_Data.csv'

data = pd.read_csv(csv_file)

# y = w*x + b
x = data['YearsExperience']
y = data['Salary']

# def plot_pred(w, b):
#     y_pred = x*w+b
#     plt.plot(x, y_pred, color='blue', label='pred')
#     plt.scatter(x, y, marker='*', color='red', label='real')
#     plt.title('Year-Salary')
#     plt.xlabel('年資')
#     plt.ylabel('月薪(千)')
#     plt.xlim([0, 12])
#     plt.ylim([-60, 140])
#     plt.legend()
#     plt.show()

# plot_pred(10, 30)

def compute_cost(x, y, w, b):
    y_pred = w*x + b
    cost = (y - y_pred)**2
    cost = cost.sum() / len(x)
    return cost

print(compute_cost(x, y, 10, 10))

costs = []
for w in range(-100, 101):
    cost = compute_cost(x, y, w, 0)
    costs.append(cost)

# The cost plot
plt.plot(range(-100, 101), costs)
plt.show()